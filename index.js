'use strict';

//1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

const paragraphs = document.querySelectorAll('p');

console.log(paragraphs);

paragraphs.forEach(paragraph => {
    paragraph.style.background = '#ff0000';
});

//2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const elem = document.getElementById('optionsList');

console.log(elem);

const elemAncestor = elem.parentElement;

console.log(elemAncestor);

console.log(elem.hasChildNodes());

console.log(elem.childNodes.length);

for (let i = 0; i < elem.childNodes.length; i++) {
    console.log(elem.childNodes[i]);
}

//3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph.

const elemTestParagraph = document.querySelector('#testParagraph');

console.log(elemTestParagraph);

elemTestParagraph.innerHTML = 'This is a paragraph';

//4. Отримати елементи
//5., вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const elemMainHeader = document.querySelector('.main-header');

console.log(elemMainHeader);

console.log(elemMainHeader.childNodes.length);

for (let i = 0; i < elemMainHeader.childNodes.length; i++) {
    console.log(elemMainHeader.childNodes[i]);
}

[...elemMainHeader.children].forEach(element => {
    element.classList.add('nav-item');
});

console.dir(elemMainHeader.childNodes);

//6. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const elemClass = document.querySelectorAll('.section-title');

console.log(elemClass);

for (let i = 0, length = elemClass.length; i < length; i++) {
    elemClass[i].classList.remove('section-title');
}
